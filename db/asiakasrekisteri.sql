drop database if exists asiakasrekisteri;

create database if not exists asiakasrekisteri;

use asiakasrekisteri;

create table if not exists asiakas (
    id int primary key auto_increment,
    sukunimi varchar(50) not null,
    etunimi varchar(50) not null,
    osoite varchar(50) not null,
    postinumero int not null,
    postitoimipaikka varchar(35) not null
);

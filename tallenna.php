<?php include_once 'inc/top.php';?>
<div class="container">
<?php
if ($_SERVER['REQUEST_METHOD']=='POST') {
    
    $sukunimi=  filter_input(INPUT_POST, 'sukunimi',FILTER_SANITIZE_STRING);
    $etunimi=  filter_input(INPUT_POST, 'etunimi',FILTER_SANITIZE_STRING);
    $osoite=  filter_input(INPUT_POST, 'osoite',FILTER_SANITIZE_STRING);
    $postinumero=  filter_input(INPUT_POST, 'postinumero',FILTER_SANITIZE_NUMBER_INT);
    $postitoimipaikka=  filter_input(INPUT_POST, 'postitoimipaikka',FILTER_SANITIZE_STRING);
    
    $kysely = $tietokanta->prepare("INSERT INTO asiakas (sukunimi,etunimi,osoite,postinumero,postitoimipaikka)"
            . "VALUES (:sukunimi,:etunimi,:osoite,:postinumero,:postitoimipaikka)");
    $kysely->bindValue(':sukunimi', $sukunimi,PDO::PARAM_STR);
    $kysely->bindValue(':etunimi',$etunimi,PDO::PARAM_STR);
    $kysely->bindValue(':osoite',$osoite,PDO::PARAM_STR);
    $kysely->bindValue(':postinumero',$postinumero,PDO::PARAM_INT);
    $kysely->bindValue(':postitoimipaikka',$postitoimipaikka,PDO::PARAM_STR);
    $kysely->execute();
    
    print "<p>Tiedot tallennettu</p>";
    print "<a href='index.php'>Takaisin etusivulle</a>";
    
}

?>

    <form role="form" action="" method='post'>
        <div class="form-group">
            <label for="sukunimi">Sukunimi:</label>
            <input type="text" class="form-control" id="sukunimi" name="sukunimi">
        </div>
        <div class="form-group">
            <label for="etunimi">Etunimi:</label>
            <input type="text" class="form-control" id="etunimi" name="etunimi">
        </div>
        <div class="form-group">
            <label for="etunimi">Lähiosoite:</label>
            <input type="text" class="form-control" id="osoite" name="osoite">
        </div>
        <div class="form-group">
            <label for="etunimi">Postinumero:</label>
            <input type="text" class="form-control" id="postinumero" name="postinumero">
        </div>
        <div class="form-group">
            <label for="etunimi">Postitoimipaikka:</label>
            <input type="text" class="form-control" id="postitoimipaikka" name="postitoimipaikka">
        </div>
        <div class="form-group">
            <input type='submit' class="btn btn-primary">
            <a href="index.php" class="btn btn-default">Peruuta</a>
        </div>
    </form>
</div>

<?php include_once 'inc/bottom.php';?>


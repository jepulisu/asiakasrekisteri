<?php include_once 'inc/top.php';?>
<?php 
$like="";
$orderby="";
if ($_SERVER['REQUEST_METHOD']=='GET') {
    $muuttuja = filter_input(INPUT_POST, 'jarjesta',FILTER_SANITIZE_STRING);
    
    if (isset($muuttuja)) {
        $orderby = $muuttuja;
    }
}
if ($_SERVER['REQUEST_METHOD']=='POST') {
    $haku = filter_input(INPUT_POST, 'haku',FILTER_SANITIZE_STRING);
    
    if (isset($haku)) {
        $like = $haku;
    }
        
}
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <form action="" method="post" role="form">
                <input type="text" id="haku" name="haku">
                <button type="submit"><span class="glyphicon glyphicon-search"></span></button>
                <!--<input type="submit" class="btn btn-info" value="Hae">-->
                <a href="tallenna.php" class="btn btn-primary">Lisää</a>
            </form>
            
            
         <?php
         $sql='SELECT * FROM asiakas';
         if (strlen($like)>0) {
             //$sql = "SELECT * FROM asiakas"
             $sql .= " WHERE etunimi like '%$like%'";
         }
         if (strlen($orderby)>0) {
             $sql .=" ORDER BY $orderby";
         }
         
         
         $kysely=$tietokanta->query($sql);
         $kysely->setFetchMode(PDO::FETCH_OBJ);
         print "<table class='table'>";
         print "<tr>";
         print "<th><a href='index.php?jarjesta=etunimi'>Etunimi</a></th><th><a href='index.php'>Sukunimi</a></th><th><a href='index.php'>Osoite</a></th><th><a href='index.php'>Postinumero</a></th><th><a href='index.php'>Postitoimipaikka</a></th>";
         print "</tr>";
         while ($tietue = $kysely->fetch()) {
             print "<tr>";
             print "<td>" . $tietue->etunimi . "</td>";
             print "<td>" . $tietue->sukunimi . "</td>";
             print "<td>" . $tietue->osoite . "</td>";
             print "<td>" . $tietue->postinumero . "</td>";
             print "<td>" . $tietue->postitoimipaikka . "</td>";
             print "</tr>";
         }
         print "</table>";
         
         print "<p>Asiakkaita: " . $kysely->rowCount() . "<p>";
         
         ?>
        </div>
    </div>
</div>
<?php include_once 'inc/bottom.php';?>